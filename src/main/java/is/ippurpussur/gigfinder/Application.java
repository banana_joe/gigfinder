package is.ippurpussur.gigfinder;

import is.ippurpussur.gigfinder.model.neo4j.*;
import is.ippurpussur.gigfinder.model.parser.SongkickParser;
import is.ippurpussur.gigfinder.model.songkick.SongkickEvent;
import is.ippurpussur.gigfinder.neo4j.service.*;
import is.ippurpussur.gigfinder.restclient.bandsintown.BandsintownAPIImpl;
import is.ippurpussur.gigfinder.restclient.lastfm.LastFmAPIImpl;
import is.ippurpussur.gigfinder.restclient.songkick.SongkickAPIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableNeo4jRepositories
@EntityScan("is.ippurpussur.gigfinder.model.neo4j")
public class Application {

    private final static Logger log = LoggerFactory.getLogger(Application.class);
    private final UserService userService;
    private final ArtistService artistService;
    private final EventService eventService;
    private final VenueService venueService;
    private final StartTimeService startTimeService;
    private final MatchLevelService matchLevelService;
    private final CoordinatesService coordinatesService;
    private final AreaService areaService;

    @Autowired
    public Application(final UserService userService,
                       final ArtistService artistService,
                       final EventService eventService,
                       final VenueService venueService,
                       final StartTimeService startTimeService,
                       final MatchLevelService matchLevelService,
                       final CoordinatesService coordinatesService,
                       final AreaService areaService) {
        this.userService = userService;
        this.artistService = artistService;
        this.eventService = eventService;
        this.venueService = venueService;
        this.startTimeService = startTimeService;
        this.matchLevelService = matchLevelService;
        this.coordinatesService = coordinatesService;
        this.areaService = areaService;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner demo() {
        return args -> {
//
            userService.deleteAll();
            artistService.deleteAll();
            eventService.deleteAll();
            venueService.deleteAll();
            startTimeService.deleteAll();
            matchLevelService.deleteAll();
            coordinatesService.deleteAll();
            areaService.deleteAll();

            areaService.createConstraint();
            artistService.createConstraint();
            coordinatesService.createConstraint();
            eventService.createSongkickIdAsUniqueConstraint();
            eventService.createBandsintownIdAsUniqueConstraint();
            startTimeService.createConstraint();
            venueService.createConstraint();
            userService.createConstraint();

            final String username = "lindabjalla";
            final LastFmAPIImpl lastFmAPI = new LastFmAPIImpl();
            final List<Artist> topArtists = lastFmAPI.getTopArtistsByLastFmUsername(username);
//
            User izumi = new User(username);
//
//            log.info("Before linking up with Neo4j...");
//            log.info("user izumi", izumi.toString());
//
            userService.create(izumi);
//
//            izumi = userService.findByName(izumi.getName());
            izumi.addTrackingArtists(new HashSet<>(topArtists));
            userService.create(izumi);
//
//            log.info("Lookup user by name...");
//            log.info(userService.findByName(izumi.getName()).toString());
//
            final Set<Artist> trackingArtists = izumi.getTrackingArtists();
//            Artist hauschka = new Artist("Hauschka", "767026a6-9e39-463b-9d04-ed0f86ac5ee7");
//            Artist peter = new Artist("Peter Broderick", "e60a4481-472a-42cf-a84d-6a9419e4e5e3");
//            Artist sigur = new Artist("Sigur Rós", "f6f2326f-6b25-4170-b89d-e235b25508e8");
//            Artist bjork = new Artist("Björk", "87c5dedd-371d-4a53-9f7f-80522fb7f3cb");
            final List<Artist> artists = new ArrayList<>(trackingArtists);
//            final List<Artist> artists = new ArrayList<>();
//            artists.add(hauschka);
//            artists.add(peter);
//            artists.add(sigur);
//            artists.add(bjork);
//
            final String languageCode = "en";

            final SongkickAPIImpl songkickAPI = new SongkickAPIImpl();
            final Set<SongkickEvent> upcomingEventsByMultipleArtists = songkickAPI.getUpcomingEventsByMultipleArtists(artists);

            final List<SongkickEvent> songkickEventsWithVenues = songkickAPI.setVenueDetailsToEvents(new ArrayList<>(upcomingEventsByMultipleArtists));

            SongkickParser songkickParser = new SongkickParser();
            final List<Event> upcomingEvents = songkickParser.convertSongkickEventsToNodeEntities(songkickEventsWithVenues, languageCode);

            eventService.createMany(upcomingEvents);
//
            final int limit = 10;
            final Set<MatchLevel> matchLevelsOfMultipleArtists = lastFmAPI.createMatchLevelsOfMultipleArtists(artists, limit);
            matchLevelService.createMany(new ArrayList<>(matchLevelsOfMultipleArtists));

            final BandsintownAPIImpl bandsintownAPI = new BandsintownAPIImpl();
            final Set<Event> bandsintownEvents =
                    bandsintownAPI.getUpcomingEventsByMultipleArtists(artists, areaService, languageCode);

            final List<Event> createdBandsintownEvents = eventService.createMany(new ArrayList<>(bandsintownEvents));
            System.out.println("createdBandsintownEvents = " + createdBandsintownEvents);
            int eventsSize = eventService.countEventsByCity("London");
            System.out.println("eventsSize = " + eventsSize);

            final Venue byLatLng = venueService.findByLatLng("36.789592,138.7836855");
            System.out.println("byLatLng = " + byLatLng);
        };
    }
}
