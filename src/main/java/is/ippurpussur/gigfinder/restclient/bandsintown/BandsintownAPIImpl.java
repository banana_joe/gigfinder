package is.ippurpussur.gigfinder.restclient.bandsintown;

import is.ippurpussur.gigfinder.model.bandsintown.BandsintownEvent;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.model.parser.BandsintownParser;
import is.ippurpussur.gigfinder.neo4j.service.AreaService;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class BandsintownAPIImpl {

    private final BandsintownAPI bandsintownAPI;

    public BandsintownAPIImpl() {
        bandsintownAPI = createService();
    }

    private BandsintownAPI createService() {

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.bandsintown.com/artists/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(BandsintownAPI.class);
    }

    private List<Event> getUpcomingEvents(final Artist artist,
                                          final AreaService areaService,
                                          final String languageCode) throws IOException {

        final List<Event> events = new ArrayList<>();
        final String mbid = artist.getMbid();
        if (mbid != null) {
            final Call<List<BandsintownEvent>> call = bandsintownAPI.getUpcomingEvents(mbid);
            final List<BandsintownEvent> bandsintownEvents = call.execute().body();
            if (bandsintownEvents != null && bandsintownEvents.size() > 0) {
                final BandsintownParser bandsintownParser = new BandsintownParser();
                bandsintownEvents.forEach(bandsintownEvent -> {
                    Event event = null;
                    try {
                        event = bandsintownParser.convertBandsintownEventToNodeEntity(
                                bandsintownEvent, areaService, languageCode
                        );
                    } catch (final IOException e) {
                        e.printStackTrace();
                    }
                    events.add(event);
                });
            }
        }
        return events;
    }

    public Set<Event> getUpcomingEventsByMultipleArtists(final List<Artist> artists,
                                                         final AreaService areaService,
                                                         final String languageCode) {

        final Set<Event> eventsByMultipleArtists = new HashSet<>();
        artists.forEach(artist -> {
            try {
                final List<Event> events = getUpcomingEvents(artist, areaService, languageCode);
                eventsByMultipleArtists.addAll(events);
            } catch (final IOException e) {
                e.printStackTrace();
            }
        });
        return eventsByMultipleArtists;
    }
}
