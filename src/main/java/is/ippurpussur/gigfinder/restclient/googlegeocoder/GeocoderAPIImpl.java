package is.ippurpussur.gigfinder.restclient.googlegeocoder;

import is.ippurpussur.gigfinder.model.googlegeocoder.AddressComponent;
import is.ippurpussur.gigfinder.model.googlegeocoder.GeocoderResponse;
import is.ippurpussur.gigfinder.model.googlegeocoder.Result;
import is.ippurpussur.gigfinder.model.parser.Key;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static is.ippurpussur.gigfinder.model.parser.Key.CITY;
import static is.ippurpussur.gigfinder.model.parser.Key.COUNTRY;

public final class GeocoderAPIImpl {

    private final GeocoderAPI geocoderAPI;

    public GeocoderAPIImpl() {
        geocoderAPI = createService();
    }

    private GeocoderAPI createService() {

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/geocode/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(GeocoderAPI.class);
    }

    private GeocoderResponse getCityNameByCoordinates(final String langLng, final String languageCode) throws IOException {
        final Call<GeocoderResponse> call = geocoderAPI.getCityNameByCoordinates(langLng, languageCode);
        return call.execute().body();
    }

    public Map<String, String> getCityAndCountry(final Double latitude, final Double longitude, final String languageCode) throws IOException {
        final String latLng = formatLatLng(latitude, longitude);
        final GeocoderResponse geocoderResponse = getCityNameByCoordinates(latLng, languageCode);

        if (geocoderResponse != null) {
            final List<AddressComponent> allAddressComponents = new ArrayList<>();
            final List<Result> results = geocoderResponse.getResults();
            results.forEach(result -> {
                final List<AddressComponent> addressComponents = result.getAddressComponents();
                allAddressComponents.addAll(addressComponents);
            });
            if (allAddressComponents.size() > 0) {
                return findLocalityAndCountry(allAddressComponents);
            }
        }
        return null;
    }

    private String formatLatLng(Double latitude, Double longitude) {
        return String.valueOf(latitude) + "," + String.valueOf(longitude);
    }

    private Map<String, String> findLocalityAndCountry(List<AddressComponent> addressComponents) {
        final String typeLocality = "locality";
        final String typeCountry = "country";
        final String typeAdminAreaLevel3 = "administrative_area_level_3";

        final List<String> localities = new ArrayList<>();
        final List<String> countries = new ArrayList<>();
        final List<String> adminAreaLevel3List = new ArrayList<>();

        final Map<String, String> localityAndCountry = new HashMap<>();

        for (final AddressComponent addressComponent : addressComponents) {
            String typeToCheck = addressComponent.getTypes().get(0);
            if (typeToCheck.equals(typeLocality)) {
                localities.add(addressComponent.getName());
            }
            if (typeToCheck.equals(typeCountry)) {
                countries.add(addressComponent.getName());
            }
            if (typeToCheck.equals(typeAdminAreaLevel3)) {
                adminAreaLevel3List.add(addressComponent.getName());
            }
        }

        final int localitiesSize = localities.size();
        final int adminAreaLevel3ListSize = adminAreaLevel3List.size();
        if (localitiesSize > 0) {
            localityAndCountry.put(CITY, localities.get(localitiesSize - 1));
        } else if (adminAreaLevel3ListSize > 0) {
            localityAndCountry.put(CITY, adminAreaLevel3List.get(adminAreaLevel3ListSize - 1));
        }

        final int countriesSize = countries.size();
        if (countriesSize > 0) {
            localityAndCountry.put(COUNTRY, countries.get(countriesSize - 1));
        }

        return localityAndCountry;
    }

    private GeocoderResponse getLatLngByAddress(final String address) throws IOException {
        final Call<GeocoderResponse> call = geocoderAPI.getLatLng(address);
        return call.execute().body();
    }

    public Map<String, Double> getLatLng(final String address) throws IOException {
        final Map<String, Double> latLng = new HashMap<>();
        final GeocoderResponse geocoderResponse = getLatLngByAddress(address);
        if (geocoderResponse != null) {
            final List<Result> results = geocoderResponse.getResults();
            if (results.size() > 0) {
                final Double lat = results.get(0).getGeometry().getLocation().getLatitude();
                final Double lng = results.get(0).getGeometry().getLocation().getLongitude();
                latLng.put(Key.LAT, lat);
                latLng.put(Key.LNG, lng);
            }
        }

        return latLng;
    }
}
