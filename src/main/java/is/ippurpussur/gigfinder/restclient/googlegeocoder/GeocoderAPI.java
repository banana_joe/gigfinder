package is.ippurpussur.gigfinder.restclient.googlegeocoder;

import is.ippurpussur.gigfinder.model.googlegeocoder.GeocoderResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeocoderAPI {

    @GET("json?key=***REMOVED***")
    Call<GeocoderResponse> getCityNameByCoordinates(
            @Query("latlng") final String latLng, @Query("language") final String languageCode);

    @GET("json?key=***REMOVED***")
    Call<GeocoderResponse> getLatLng(@Query("address") final String address);
}
