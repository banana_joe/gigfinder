package is.ippurpussur.gigfinder.restclient.songkick;

import is.ippurpussur.gigfinder.model.songkick.SongkickResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface SongkickAPI {

    @GET("artists/mbid:{artistMbid}/calendar.json?apikey=***REMOVED***")
    Call<SongkickResponse> getUpcomingEvents(@Path("artistMbid") final String artistMbid);

    @GET("venues/{venueId}.json?apikey=***REMOVED***")
    Call<SongkickResponse> getVenueDetails(@Path("venueId") final Long venueId);
}
