package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public final class Results {

    @SerializedName("event")
    private List<SongkickEvent> events = new ArrayList<>();

    private SongkickVenue venue;

    public List<SongkickEvent> getEvents() {
        return events;
    }

    public SongkickVenue getVenue() {
        return venue;
    }

    @Override
    public String toString() {
        return "Result{" +
                "events=" + events +
                ", venue=" + venue +
                '}';
    }
}
