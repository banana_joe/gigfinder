package is.ippurpussur.gigfinder.model.songkick;

public final class SongkickResponse {

    private ResultPage resultsPage;

    public ResultPage getResultsPage() {
        return resultsPage;
    }

    @Override
    public String toString() {
        return "SongkickResponse{" +
                "resultsPage=" + resultsPage +
                '}';
    }
}
