package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;
import is.ippurpussur.gigfinder.model.neo4j.StartTime;

import java.util.List;

public final class SongkickEvent {

    @SerializedName("id")
    private final Long songkickId;

    @SerializedName("displayName")
    private final String name;

    @SerializedName("performance")
    private final List<Performance> performanceList;

    private SongkickVenue venue;

    @SerializedName("start")
    private StartTime startTime;

    public SongkickEvent(final Long songkickId,
                         final String name,
                         final List<Performance> performanceList,
                         final SongkickVenue venue,
                         final StartTime startTime) {
        this.songkickId = songkickId;
        this.name = name;
        this.performanceList = performanceList;
        this.venue = venue;
        this.startTime = startTime;
    }

    public Long getSongkickId() {
        return songkickId;
    }

    public String getName() {
        return name;
    }

    public List<Performance> getPerformanceList() {
        return performanceList;
    }

    public SongkickVenue getVenue() {
        return venue;
    }

    public StartTime getStartTime() {
        return startTime;
    }

    public void setVenue(final SongkickVenue venue) {
        this.venue = venue;
    }

    @Override
    public String toString() {
        return "SongkickEvent{" +
                "name='" + name + '\'' +
                ", performanceList=" + performanceList +
                ", venue=" + venue +
                ", startTime=" + startTime +
                '}';
    }
}
