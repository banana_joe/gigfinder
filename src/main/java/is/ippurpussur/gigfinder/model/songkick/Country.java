package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;

public final class Country {

    @SerializedName("displayName")
    private String name;

    public Country(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                '}';
    }
}
