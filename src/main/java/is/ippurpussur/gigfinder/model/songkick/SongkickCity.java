package is.ippurpussur.gigfinder.model.songkick;

import com.google.gson.annotations.SerializedName;

public final class SongkickCity {

    @SerializedName("displayName")
    private final String name;

    private final Country country;

    public SongkickCity(final String name, final Country country){
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "SongkickCity{" +
                "name='" + name + '\'' +
                ", country=" + country +
                '}';
    }
}
