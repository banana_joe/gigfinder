package is.ippurpussur.gigfinder.model.songkick;

public final class Identifier {

    private final String mbid;

    public Identifier(final String mbid){
        this.mbid = mbid;
    }

    public String getMbid() {
        return mbid;
    }

    @Override
    public String toString() {
        return "Identifier{" +
                "mbid='" + mbid + '\'' +
                '}';
    }
}
