package is.ippurpussur.gigfinder.model.googlegeocoder;

import java.util.List;

public final class GeocoderResponse {

    private List<Result> results;

    public List<Result> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return "GeocoderResponse{" +
                "results=" + results +
                '}';
    }
}
