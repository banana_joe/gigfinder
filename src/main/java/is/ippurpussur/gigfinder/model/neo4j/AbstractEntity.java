package is.ippurpussur.gigfinder.model.neo4j;

import org.neo4j.ogm.annotation.GraphId;

public abstract class AbstractEntity {

    @GraphId
    Long id;

    String uuid;

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
