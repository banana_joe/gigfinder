package is.ippurpussur.gigfinder.model.neo4j;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "SIMILAR_TO")
public class MatchLevel extends AbstractEntity {

    @Property
    private Double level;

    @StartNode
    private Artist artist;

    @EndNode
    private Artist similarArtist;

    private MatchLevel() {
    }

    public MatchLevel(final Double level, final Artist artist, final Artist similarArtist) {
        this.level = level;
        this.artist = artist;
        this.similarArtist = similarArtist;
    }

    public MatchLevel(final MatchLevel another) {
        this.id = another.id;
        this.uuid = another.uuid;
        this.level = another.level;
        this.artist = another.artist;
        this.similarArtist = another.similarArtist;
    }

    public Double getLevel() {
        return level;
    }

    public Artist getArtist() {
        return artist;
    }

    public Artist getSimilarArtist() {
        return similarArtist;
    }

    public void setArtist(final Artist artist) {
        this.artist = artist;
    }

    public void setSimilarArtist(final Artist similarArtist) {
        this.similarArtist = similarArtist;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other instanceof MatchLevel) {
            final MatchLevel otherMatchLevel = (MatchLevel) other;
            return (uuid != null ? uuid.equals(otherMatchLevel.uuid) : otherMatchLevel.uuid == null)
                    && (level != null ? level.equals(otherMatchLevel.level) : otherMatchLevel.level == null)
                    && (artist != null ? artist.equals(otherMatchLevel.artist) : otherMatchLevel.artist == null)
                    && (similarArtist != null
                    ? similarArtist.equals(otherMatchLevel.similarArtist) : otherMatchLevel.similarArtist == null);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result += (uuid != null ? uuid.hashCode() : 0) * 37;
        result += (level != null ? level.hashCode() : 0) * 37;
        result += (artist != null ? artist.hashCode() : 0) * 37;
        result += (similarArtist != null ? similarArtist.hashCode() : 0) * 37;

        return result;
    }

    @Override
    public String toString() {
        return "MatchLevel{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", level=" + level +
                ", artist=" + artist +
                ", similarArtist=" + similarArtist +
                '}';
    }
}
