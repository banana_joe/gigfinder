package is.ippurpussur.gigfinder.model.bandsintown;

import com.google.gson.annotations.SerializedName;
import is.ippurpussur.gigfinder.model.neo4j.Artist;

import java.util.List;

public final class BandsintownEvent {

    @SerializedName("id")
    private Long bandsintownId;

    @SerializedName("title")
    private String name;

    @SerializedName("datetime")
    private String dateTime;

    private List<Artist> artists;

    private BandsintownVenue venue;

    public BandsintownEvent(final Long bandsintownId,
                            final String name,
                            final String dateTime,
                            final List<Artist> artists,
                            final BandsintownVenue venue) {
        this.bandsintownId = bandsintownId;
        this.name = name;
        this.dateTime = dateTime;
        this.artists = artists;
        this.venue = venue;
    }

    public Long getBandsintownId() {
        return bandsintownId;
    }

    public String getName() {
        return name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public BandsintownVenue getVenue() {
        return venue;
    }
}
