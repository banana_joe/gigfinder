package is.ippurpussur.gigfinder.model.lastfm;

import com.google.gson.annotations.SerializedName;

public final class LastFmSimilarArtistsResponse {

    @SerializedName("similarartists")
    private SimilarArtists similarArtists;

    public SimilarArtists getSimilarArtists() {
        return similarArtists;
    }

    @Override
    public String toString() {
        return "LastFmSimilarArtistsResponse{" +
                "similarArtists=" + similarArtists +
                '}';
    }
}
