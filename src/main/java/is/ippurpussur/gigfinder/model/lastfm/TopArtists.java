package is.ippurpussur.gigfinder.model.lastfm;

import com.google.gson.annotations.SerializedName;
import is.ippurpussur.gigfinder.model.neo4j.Artist;

import java.util.List;

public final class TopArtists {

    @SerializedName("artist")
    private List<Artist> artists;

    public List<Artist> getArtists() {
        return artists;
    }

    @Override
    public String toString() {
        return "TopArtists{" +
                "artists=" + artists +
                '}';
    }
}
