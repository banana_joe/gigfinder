package is.ippurpussur.gigfinder.model.lastfm;

import com.google.gson.annotations.SerializedName;

public final class LastFmTopArtistsResponse {

    @SerializedName("topartists")
    private TopArtists topArtists;

    public TopArtists getTopArtists() {
        return topArtists;
    }

    @Override
    public String toString() {
        return "LastFmTopArtistsResponse{" +
                "topArtists=" + topArtists +
                '}';
    }
}
