package is.ippurpussur.gigfinder.webservice;

import is.ippurpussur.gigfinder.model.neo4j.Event;
import is.ippurpussur.gigfinder.model.webserviceresponse.EventsResult;
import is.ippurpussur.gigfinder.neo4j.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("v1/event")
@RestController
public final class EventController {

    private EventService eventService;

    @Autowired
    private EventController(final EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(method = GET)
    private EventsResult getAllEvents(
            @RequestParam(value = "page", defaultValue = "1") final int pageNumber,
            @RequestParam(value = "results", defaultValue = "10") final int numberOfResultsPerPage) {
        final List<Event> sortedAndPagedEvents = eventService.findAllSortByStartDateTime(pageNumber, numberOfResultsPerPage);
        final int depth = 3;
        final List<Event> events = getEventsWithNestedObjects(sortedAndPagedEvents, depth);
        final int numberOfAllEvents = eventService.countAllEvents();

        return new EventsResult(events, pageNumber, numberOfResultsPerPage, numberOfAllEvents);
    }

    @RequestMapping(path = "city/{cityName}", method = GET)
    private EventsResult getEventsByCity(@PathVariable final String cityName,
                                         @RequestParam(value = "page", defaultValue = "1") final int pageNumber,
                                         @RequestParam(value = "results", defaultValue = "10") final int numberOfResultsPerPage) {
        final List<Event> eventsByCity = eventService.findByCity(cityName, pageNumber, numberOfResultsPerPage);
        final int depth = 3;
        final List<Event> events = getEventsWithNestedObjects(eventsByCity, depth);
        final int numberOfEventsByCity = eventService.countEventsByCity(cityName);

        return new EventsResult(events, pageNumber, numberOfResultsPerPage, numberOfEventsByCity);
    }

    private List<Event> getEventsWithNestedObjects(final List<Event> events, final int depth) {
        final List<Event> eventsWithNestedEntities = new ArrayList<>();
        events.forEach(event -> {
            final Event eventById = eventService.findById(event.getId(), depth);
            eventsWithNestedEntities.add(eventById);
        });
        return eventsWithNestedEntities;
    }
}
