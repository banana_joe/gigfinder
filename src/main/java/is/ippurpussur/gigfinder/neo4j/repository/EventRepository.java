package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.Event;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface EventRepository extends GraphRepository<Event> {

    @Query("CREATE (e:Event{" +
            "name: {name}, " +
            "uuid: apoc.create.uuid(), " +
            "songkickId: {songkickId}, " +
            "bandsintownId: {bandsintownId}}) " +
            "RETURN e")
    Event create(@Param("name") final String name,
                 @Param("songkickId") final Long songkickId,
                 @Param("bandsintownId") final Long bandsintownId);

    @Query("MERGE (e:Event{uuid: {uuid}}) " +
            "SET e.songkickId = {songkickId}, e.bandsintownId = {bandsintownId}, e.name = {name} RETURN e")
    Event merge(@Param("uuid") final String uuid,
                @Param("songkickId") final Long songkickId,
                @Param("bandsintownId") final Long bandsintownId,
                @Param("name") final String name);

    @Query("MATCH (e:Event{uuid: {eventUuid}}), (a:Artist{uuid: {artistUuid}}) MERGE (a)-[:PERFORMS]->(e) RETURN DISTINCT e")
    Event updateRelationshipWithArtist(
            @Param("eventUuid") final String eventUuid, @Param("artistUuid") final String artistUuid);

    @Query("MATCH (e:Event{uuid: {eventUuid}}), (v:Venue{uuid: {venueUuid}}) MERGE (e)-[:HELD_IN]->(v) RETURN DISTINCT e")
    Event updateRelationshipWithVenue(@Param("eventUuid") final String eventUuId, @Param("venueUuid") final String venueUuid);

    @Query("MATCH (e:Event{uuid: {eventUuid}}), (s:StartTime{uuid:{startTimeUuid}}) MERGE (e)-[:STARTS_AT]->(s) " +
            "RETURN DISTINCT e")
    Event updateRelationshipWithStartTime(
            @Param("eventUuid") final String eventId, @Param("startTimeUuid") final String startTimeId);

    Collection<Event> findByName(final String name);

    @Query("MATCH (e:Event) WHERE e.name =~ ('(?i).*'+{name}+'.*') RETURN e")
    Collection<Event> findByNameLike(@Param("name") final String name);

    Collection<Event> findByStartTimeDateAndStartTimeTime(final String date, final String time);

    @Query("MATCH (e:Event)--(a:Artist) WHERE a.name = {name} RETURN e")
    Collection<Event> findByArtistName(@Param("name") final String name);

    Collection<Event> findByStartTimeDate(final String date);

    @Query("MATCH (s:StartTime)--(e:Event)--(v:Venue)--(a:Area) WHERE a.city =~ ('(?i)'+ {city}) RETURN DISTINCT e, s " +
            "ORDER BY s.dateTime SKIP {skip} LIMIT {limit}")
    Collection<Event> findByCity(
            @Param("city") final String city, @Param("skip") final int skip, @Param("limit") final int limit);

    @Query("MATCH (e:Event)--(s:StartTime) RETURN e ORDER BY s.dateTime SKIP {skip} LIMIT {limit}")
    Collection<Event> findAllSortByStartDateTime(@Param("skip") final int skip, @Param("limit") final int limit);

    @Query("MATCH (e:Event)--(v:Venue)--(a:Area) WHERE a.city =~ ('(?i)'+ {city}) AND e.name =~ ('(?i).*'+ {name}+'.*') " +
            "RETURN e")
    Collection<Event> findByCityAndNameLike(@Param("city") final String city, @Param("name") final String name);

    @Query("MATCH (e:Event)--(v:Venue)--(a:Area) WHERE a.city =~ ('(?i)'+ {city}) RETURN count(DISTINCT e)")
    int countEventsByCity(@Param("city") final String city);

    @Query("MATCH (e:Event) RETURN count(e)")
    int countAllEvents();

    Event findBySongkickId(final Long songkickId);

    Event findByBandsintownId(final Long bandsintownId);

    @Query("CREATE CONSTRAINT ON (e:Event) ASSERT e.songkickId IS UNIQUE")
    void createSongkickIdAsUniqueConstraint();

    @Query("CREATE CONSTRAINT ON (e:Event) ASSERT e.bandsintownId IS UNIQUE")
    void createBandsintownIdAsUniqueConstraint();
}
