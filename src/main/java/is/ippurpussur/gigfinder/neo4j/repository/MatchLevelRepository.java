package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface MatchLevelRepository extends GraphRepository<MatchLevel> {

    @Query("MATCH (a1:Artist{uuid:{artistUuid}}), (a2:Artist{uuid:{similarArtistUuid}}) MERGE (a1)-[m:SIMILAR]->(a2) " +
            "ON CREATE SET m.uuid = apoc.create.uuid(), m.level = {level} RETURN m")
    MatchLevel create(@Param("artistUuid") final String artist1Uuid,
                      @Param("similarArtistUuid") final String artist2Uuid,
                      @Param("level") final Double level);

    @Query("MATCH (a1:Artist{uuid:{artistUuid}}), (a2:Artist{uuid:{similarArtistUuid}}) MERGE (a1)-[m:SIMILAR]->(a2) " +
            "SET m.uuid = {matchLevelUuid}, m.level = {level} RETURN m")
    MatchLevel merge(@Param("artistUuid") final String artistUuid,
                     @Param("similarArtistUuid") final String similarArtistUuid,
                     @Param("matchLevelUuid") final String matchLevelUuid,
                     @Param("level") final Double level);
}
