package is.ippurpussur.gigfinder.neo4j.repository;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface ArtistRepository extends GraphRepository<Artist> {

    @Query("CREATE (a:Artist {name: {name}, uuid: apoc.create.uuid(), mbid:{mbid}, website: {website}}) RETURN a ")
    Artist create(
            @Param("name") final String name, @Param("mbid") final String mbid, @Param("website") final String website);

    @Query("MERGE (a:Artist {uuid: {uuid}}) " +
            "SET a.uuid = {uuid}, a.name = {name}, a.mbid = {mbid}, a.website = {website} RETURN a")
    Artist merge(@Param("uuid") final String uuid,
                 @Param("name") final String name,
                 @Param("mbid") final String mbid,
                 @Param("website") final String website);

    Artist findByMbid(final String mbid);

    Collection<Artist> findByName(final String name);

    @Query("CREATE CONSTRAINT ON (a:Artist) ASSERT a.mbid IS UNIQUE")
    void createConstraint();
}
