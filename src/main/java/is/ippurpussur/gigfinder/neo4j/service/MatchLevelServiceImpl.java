package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("MatchLevelService")
public final class MatchLevelServiceImpl extends CommonService implements MatchLevelService {

    private final MatchLevelRepository matchLevelRepository;

    @Autowired
    public MatchLevelServiceImpl(final VenueRepository venueRepository,
                                 final StartTimeRepository startTimeRepository,
                                 final AreaRepository areaRepository,
                                 final CoordinatesRepository coordinatesRepository,
                                 final ArtistRepository artistRepository,
                                 final EventRepository eventRepository,
                                 final MatchLevelRepository matchLevelRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
        this.matchLevelRepository = matchLevelRepository;
    }

    @Transactional
    @Override
    public MatchLevel create(final MatchLevel matchLevel) {
        final Artist artist = createArtist(matchLevel.getArtist());
        final Artist similarArtist = createArtist(matchLevel.getSimilarArtist());

        matchLevel.setArtist(artist);
        matchLevel.setSimilarArtist(similarArtist);

        return merge(matchLevel);
    }

    @Override
    public MatchLevel merge(final MatchLevel matchLevel) {
        final String matchLevelUuid = matchLevel.getUuid();
        final String artistUuid = matchLevel.getArtist().getUuid();
        final String similarArtistUuid = matchLevel.getSimilarArtist().getUuid();
        final Double level = matchLevel.getLevel();

        if (matchLevelUuid != null) {
            return matchLevelRepository.merge(artistUuid, similarArtistUuid, matchLevelUuid, level);
        }
        return matchLevelRepository.create(artistUuid, similarArtistUuid, level);
    }

    @Override
    public void delete(final MatchLevel matchLevel) {
        matchLevelRepository.delete(matchLevel);
    }

    @Override
    public MatchLevel findById(final Long id, final int depth) {
        return matchLevelRepository.findOne(id, depth);
    }

    @Override
    public void deleteAll() {
        matchLevelRepository.deleteAll();
    }

    @Transactional
    @Override
    public List<MatchLevel> createMany(final List<MatchLevel> matchLevels) {
        final List<MatchLevel> matchLevelList = new ArrayList<>();
        matchLevels.forEach(matchLevel -> {
            final MatchLevel createdMatchLevel = create(matchLevel);
            matchLevelList.add(createdMatchLevel);
        });
        return matchLevelList;
    }

    @Override
    public List<MatchLevel> findAll() {
        final Iterable<MatchLevel> matchLevels = matchLevelRepository.findAll();
        return Lists.newArrayList(matchLevels);
    }
}
