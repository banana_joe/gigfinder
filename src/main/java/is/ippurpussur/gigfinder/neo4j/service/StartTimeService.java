package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.StartTime;

import java.util.List;

public interface StartTimeService extends GenericService<StartTime> {

    List<StartTime> findByDate(final String date);

    StartTime findByDateTime(final String dateTime);

    void createConstraint();
}
