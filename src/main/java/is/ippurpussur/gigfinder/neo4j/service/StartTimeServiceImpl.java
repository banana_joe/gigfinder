package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.StartTime;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("StartTimeService")
public final class StartTimeServiceImpl extends CommonService implements StartTimeService {

    @Autowired
    public StartTimeServiceImpl(final VenueRepository venueRepository,
                                final StartTimeRepository startTimeRepository,
                                final AreaRepository areaRepository,
                                final CoordinatesRepository coordinatesRepository,
                                final ArtistRepository artistRepository,
                                final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Override
    public StartTime create(final StartTime startTime) {
        return createStartTime(startTime);
    }

    @Override
    public StartTime merge(final StartTime startTime) {
        return mergeStartTime(startTime);
    }

    @Override
    public void delete(final StartTime startTime) {
        startTimeRepository.delete(startTime);
    }

    @Override
    public StartTime findById(final Long id, final int depth) {
        return startTimeRepository.findOne(id, depth);
    }

    @Override
    public List<StartTime> findAll() {
        final Iterable<StartTime> startTimes = startTimeRepository.findAll();
        return Lists.newArrayList(startTimes);
    }

    @Override
    public void deleteAll() {
        startTimeRepository.deleteAll();
    }

    @Override
    public List<StartTime> findByDate(final String date) {
        return findStartTimesByDate(date);
    }

    @Override
    public StartTime findByDateTime(final String dateTime) {
        return findStartTimeByDateTime(dateTime);
    }

    @Override
    public void createConstraint() {
        startTimeRepository.createConstraint();
    }
}
