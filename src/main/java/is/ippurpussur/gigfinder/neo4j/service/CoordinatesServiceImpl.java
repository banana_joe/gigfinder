package is.ippurpussur.gigfinder.neo4j.service;

import com.google.common.collect.Lists;
import is.ippurpussur.gigfinder.model.neo4j.Coordinates;
import is.ippurpussur.gigfinder.neo4j.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CoordinatesService")
public final class CoordinatesServiceImpl extends CommonService implements CoordinatesService {

    @Autowired
    public CoordinatesServiceImpl(final VenueRepository venueRepository,
                                  final StartTimeRepository startTimeRepository,
                                  final AreaRepository areaRepository,
                                  final CoordinatesRepository coordinatesRepository,
                                  final ArtistRepository artistRepository,
                                  final EventRepository eventRepository) {
        super(venueRepository, startTimeRepository, areaRepository, coordinatesRepository, artistRepository, eventRepository);
    }

    @Override
    public Coordinates create(final Coordinates coordinates) {
        return createCoordinates(coordinates);
    }

    @Override
    public Coordinates merge(final Coordinates coordinates) {
        return mergeCoordinates(coordinates);
    }

    @Override
    public void delete(final Coordinates coordinates) {
        coordinatesRepository.delete(coordinates);
    }

    @Override
    public Coordinates findById(final Long id, final int depth) {
        return coordinatesRepository.findOne(id, depth);
    }

    @Override
    public List<Coordinates> findAll() {
        final Iterable<Coordinates> coordinates = coordinatesRepository.findAll();
        return Lists.newArrayList(coordinates);
    }

    @Override
    public void deleteAll() {
        coordinatesRepository.deleteAll();
    }

    @Override
    public Coordinates findByFormattedCoordinates(final Double formattedLatitude, final Double formattedLongitude) {
        return findCoordinatesByFormattedCoordinates(formattedLatitude, formattedLongitude);
    }

    @Override
    public Coordinates findByLatLng(String latLng) {
        return findCoordinatesByLatLng(latLng);
    }

    @Override
    public void createConstraint() {
        coordinatesRepository.createConstraint();
    }
}
