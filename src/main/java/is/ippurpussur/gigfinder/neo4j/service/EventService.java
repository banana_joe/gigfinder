package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Event;

import java.util.List;

public interface EventService extends GenericService<Event> {

    List<Event> createMany(final List<Event> events);

    List<Event> findByName(final String name);

    List<Event> findByNameLike(final String name);

    List<Event> findByStartDateAndStartTime(final String date, final String time);

    List<Event> findByArtistName(final String name);

    List<Event> findByStartDate(final String date);

    List<Event> findByCity(final String city, final int pageNumber, final int numberOfResultsPerPage);

    List<Event> findAllSortByStartDateTime(final int pageNumber, final int numberOfResultsPerPage);

    List<Event> findByCityAndNameLike(final String city, final String name);

    int countEventsByCity(final String city);

    int countAllEvents();

    Event findBySongkickId(final Long songkickId);

    Event findByBandsintownId(final Long bandsintownId);

    void createSongkickIdAsUniqueConstraint();

    void createBandsintownIdAsUniqueConstraint();
}
