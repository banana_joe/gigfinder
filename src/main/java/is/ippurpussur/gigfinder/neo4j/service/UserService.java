package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.User;

public interface UserService extends GenericService<User> {

    User findByName(final String name);

    void createConstraint();
}
