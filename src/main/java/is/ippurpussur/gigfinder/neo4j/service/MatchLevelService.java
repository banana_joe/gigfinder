package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;

import java.util.List;

public interface MatchLevelService extends GenericService<MatchLevel> {

    List<MatchLevel> createMany(final List<MatchLevel> matchLevels);
}
