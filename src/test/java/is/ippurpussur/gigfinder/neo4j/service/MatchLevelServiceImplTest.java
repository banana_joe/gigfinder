package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.model.neo4j.MatchLevel;
import is.ippurpussur.gigfinder.neo4j.repository.ArtistRepository;
import is.ippurpussur.gigfinder.neo4j.repository.MatchLevelRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.ARTIST_PETER_BRODERICK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MatchLevelServiceImplTest extends AbstractServiceImplTest<MatchLevel, MatchLevelRepository, MatchLevelService> {

    @InjectMocks
    private MatchLevelServiceImpl matchLevelService;

    @Mock
    private MatchLevelRepository matchLevelRepositoryMock;

    @Mock
    private ArtistRepository artistRepositoryMock;

    private MatchLevel matchLevel;

    private MatchLevel matchLevel2;

    private MatchLevel matchLevelWithId;

    private MatchLevel matchLevelWithId2;

    private List<MatchLevel> matchLevels;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(matchLevelRepositoryMock);
        MockitoAnnotations.initMocks(artistRepositoryMock);

        final Artist artist = new Artist(ARTIST_PETER_BRODERICK);
        artist.setId(5L);
        artist.setUuid("5");

        final Artist similarArtist = new Artist("Greg Haines", "062b9fb1-4bb9-40b2-80a1-9bbf2be2d2cd");
        similarArtist.setId(3L);
        similarArtist.setUuid("3");

        final Artist similarArtist2 = new Artist("A Winged Victory for the Sullen", "67c179d3-f358-43bf-8ecc-d9c560878b65");
        similarArtist2.setId(4L);
        similarArtist2.setUuid("4");

        matchLevel = new MatchLevel(0.881597, artist, similarArtist);
        matchLevel2 = new MatchLevel(0.898263, artist, similarArtist2);

        matchLevelWithId = new MatchLevel(matchLevel);
        matchLevelWithId.setId(1L);

        matchLevelWithId2 = new MatchLevel(matchLevel2);
        matchLevelWithId2.setId(2L);

        matchLevels = new ArrayList<>(Arrays.asList(matchLevelWithId, matchLevelWithId2));

        final String artistUuid = artist.getUuid();
        final String similarArtistUuid = similarArtist.getUuid();
        final String similarArtist2Uuid = similarArtist2.getUuid();

        when(matchLevelRepositoryMock.create(artistUuid, similarArtistUuid, matchLevel.getLevel()))
                .thenReturn(matchLevelWithId);

        when(matchLevelRepositoryMock.create(artistUuid, similarArtist2Uuid, matchLevel2.getLevel()))
                .thenReturn(matchLevelWithId2);

        when(artistRepositoryMock.merge(artistUuid, artist.getName(), artist.getMbid(), artist.getWebsite()))
                .thenReturn(artist);

        when(artistRepositoryMock
                .merge(similarArtistUuid, similarArtist.getName(), similarArtist.getMbid(), similarArtist.getWebsite()))
                .thenReturn(similarArtist);

        when(artistRepositoryMock
                .merge(similarArtist2Uuid, similarArtist2.getName(), similarArtist2.getMbid(), similarArtist2.getWebsite()))
                .thenReturn(similarArtist2);
    }

    @Test
    public void shouldCreate() throws Exception {
        final MatchLevel createdMatchLevel = matchLevelService.create(matchLevel);
        final MatchLevel createdMatchLevel2 = matchLevelService.create(matchLevel);
        final MatchLevel createdMatchLevel3 = matchLevelService.create(matchLevel2);

        assertThat(createdMatchLevel, equalTo(matchLevelWithId));
        assertThat(createdMatchLevel2, equalTo(createdMatchLevel));
        assertThat(createdMatchLevel3, not(equalTo(createdMatchLevel)));
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(matchLevelRepositoryMock, matchLevelWithId, matchLevelService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(matchLevels, matchLevelRepositoryMock, matchLevelService);
    }

    @Test
    public void shouldCreateMany() throws Exception {
        final List<MatchLevel> matchLevels = matchLevelService.createMany(Arrays.asList(matchLevel, matchLevel2));

        assertThat(matchLevels, hasSize(2));
        assertThat(matchLevels, hasItems(matchLevelWithId, matchLevelWithId2));
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(matchLevelRepositoryMock, matchLevels, matchLevelService);
    }
}
