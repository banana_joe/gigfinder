package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.StartTime;
import is.ippurpussur.gigfinder.neo4j.repository.EventRepository;
import is.ippurpussur.gigfinder.neo4j.repository.StartTimeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StartTimeServiceImplTest extends AbstractServiceImplTest<StartTime, StartTimeRepository, StartTimeServiceImpl> {

    @InjectMocks
    private StartTimeServiceImpl startTimeService;

    @Mock
    private StartTimeRepository startTimeRepositoryMock;

    @Mock
    private EventRepository eventRepositoryMock;

    private StartTime startTime;

    private StartTime anotherStartTime;

    private StartTime startTimeWithId;

    private StartTime startTimeWithId2;

    private List<StartTime> startTimes;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(startTimeRepositoryMock);
        MockitoAnnotations.initMocks(eventRepositoryMock);

        startTime = new StartTime(DATE_TIME, DATE, TIME);
        anotherStartTime = new StartTime(ANOTHER_DATE_TIME, DATE, ANOTHER_TIME);

        startTimeWithId = new StartTime(startTime);
        startTimeWithId.setId(1L);
        startTimeWithId.setUuid("1");

        startTimeWithId2 = new StartTime(anotherStartTime);
        startTimeWithId2.setId(2L);
        startTimeWithId2.setUuid("2");

        startTimes = new ArrayList<>(Arrays.asList(startTimeWithId, startTimeWithId2));
    }

    @Test
    public void shouldCreate() throws Exception {
        when(startTimeRepositoryMock.create(DATE_TIME, DATE, TIME)).thenReturn(startTimeWithId);
        when(startTimeRepositoryMock.create(ANOTHER_DATE_TIME, DATE, TIME)).thenReturn(startTimeWithId2);
        when(eventRepositoryMock.findByStartTimeDateAndStartTimeTime(DATE, TIME)).thenReturn(new ArrayList<>());
        when(startTimeRepositoryMock.findOne(1L)).thenReturn(startTimeWithId);
        when(startTimeRepositoryMock.findByDateTime(DATE_TIME)).thenReturn(startTimeWithId);
        when(startTimeRepositoryMock.findByDate(DATE)).thenReturn(Collections.singletonList(startTimeWithId));
        when(startTimeRepositoryMock.merge("1", DATE_TIME, DATE, TIME)).thenReturn(startTimeWithId);

        final StartTime startTime = startTimeService.create(this.startTime);
        final StartTime startTime2 = startTimeService.create(this.startTime);
        final StartTime startTime3 = startTimeService.create(anotherStartTime);

        assertThat(startTime, equalTo(startTimeWithId));
        assertThat(startTime2, equalTo(startTime));
        assertThat(startTime3, not(equalTo(startTime)));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(startTimes, startTimeWithId, startTimeWithId2, startTimeRepositoryMock, startTimeService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(startTimeRepositoryMock, startTimeWithId, startTimeService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(startTimeRepositoryMock, startTimes, startTimeService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(startTimes, startTimeRepositoryMock, startTimeService);
    }

    @Test
    public void shouldFindByDate() throws Exception {
        when(startTimeRepositoryMock.findByDate(DATE)).thenReturn(Collections.singletonList(startTimeWithId));

        final List<StartTime> startTimesByDate = startTimeService.findByDate(DATE);

        assertThat(startTimesByDate, hasSize(1));
        assertThat(startTimesByDate, hasItems(startTimeWithId));
    }
}