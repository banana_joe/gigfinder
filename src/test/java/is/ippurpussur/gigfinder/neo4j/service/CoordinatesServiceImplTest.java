package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Coordinates;
import is.ippurpussur.gigfinder.neo4j.repository.CoordinatesRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoordinatesServiceImplTest extends AbstractServiceImplTest<Coordinates, CoordinatesRepository, CoordinatesServiceImpl> {

    @InjectMocks
    private CoordinatesServiceImpl coordinatesService;

    @Mock
    private CoordinatesRepository coordinatesRepositoryMock;

    private Coordinates coordinatesWithId;

    private Coordinates coordinatesWithId2;

    private List<Coordinates> coordinatesList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(coordinatesRepositoryMock);

        coordinatesWithId = new Coordinates(COORDINATES);
        coordinatesWithId.setId(1L);

        coordinatesWithId2 = new Coordinates(COORDINATES);
        coordinatesWithId2.setId(2L);

        coordinatesList = new ArrayList<>(Arrays.asList(coordinatesWithId, coordinatesWithId2));
    }

    @Test
    public void shouldCreate() throws Exception {
        when(coordinatesRepositoryMock
                .create(COORDINATES.getLatLng(), LATITUDE, LONGITUDE, FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(coordinatesWithId);
        when(coordinatesRepositoryMock
                .create(COORDINATES_2.getLatLng(), LATITUDE, LONGITUDE, FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(coordinatesWithId2);

        final Coordinates coordinates = coordinatesService.create(COORDINATES);
        final Coordinates coordinates2 = coordinatesService.create(COORDINATES);
        final Coordinates coordinates3 = coordinatesService.create(coordinatesWithId2);

        assertThat(coordinates, equalTo(coordinatesWithId));
        assertThat(coordinates2, equalTo(coordinates));
        assertThat(coordinates3.getId(), equalTo(coordinates.getId()));
        assertThat(coordinates3.getLatLng(), equalTo(coordinates.getLatLng()));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(coordinatesList, coordinatesWithId, coordinatesWithId2, coordinatesRepositoryMock, coordinatesService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(coordinatesRepositoryMock, coordinatesWithId, coordinatesService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(coordinatesRepositoryMock, coordinatesList, coordinatesService);
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(coordinatesList, coordinatesRepositoryMock, coordinatesService);
    }

    @Test
    public void shouldFindByFormattedCoordinates() throws Exception {
        when(coordinatesRepositoryMock.findByFormattedLatitudeAndFormattedLongitude(FORMATTED_LATITUDE, FORMATTED_LONGITUDE))
                .thenReturn(coordinatesWithId);

        final Coordinates coordinates = coordinatesService.findByFormattedCoordinates(FORMATTED_LATITUDE, FORMATTED_LONGITUDE);

        assertThat(coordinates, equalTo(coordinatesWithId));
    }
}