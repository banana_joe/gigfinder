package is.ippurpussur.gigfinder.neo4j.service;

import is.ippurpussur.gigfinder.model.neo4j.Artist;
import is.ippurpussur.gigfinder.neo4j.repository.ArtistRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static is.ippurpussur.gigfinder.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArtistServiceImplTest extends AbstractServiceImplTest<Artist, ArtistRepository, ArtistServiceImpl> {

    @InjectMocks
    private ArtistServiceImpl artistService;

    @Mock
    private ArtistRepository artistRepositoryMock;

    private Artist artistWithId;

    private Artist artistWithId2;

    private List<Artist> artists;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(ArtistRepository.class);

        artistWithId = new Artist(ARTIST_MUM);
        artistWithId.setId(1L);

        artistWithId2 = new Artist(ARTIST_PETER_BRODERICK);
        artistWithId2.setId(2L);

        artists = new ArrayList<>(Arrays.asList(artistWithId, artistWithId2));

        when(artistRepositoryMock.create(MUM_NAME, MUM_MBID, null)).thenReturn(artistWithId);
    }

    @Test
    public void shouldCreate() throws Exception {
        final Artist artistWithIdAndWebsite = new Artist(artistWithId);
        final String website = "mum.is";
        artistWithIdAndWebsite.setWebsite(website);

        when(artistRepositoryMock.findByMbid(MUM_MBID)).thenReturn(artistWithId);
        when(artistRepositoryMock.create(MUM_NAME, MUM_MBID, website)).thenReturn(artistWithId);

        final Artist artist = artistService.create(ARTIST_MUM);
        final Artist artist2 = artistService.create(ARTIST_MUM);
        final Artist artist3 = artistService.create(artistWithIdAndWebsite);

        assertThat(artist, equalTo(artistWithId));
        assertThat(artist2, equalTo(artist));
        assertThat(artist3.getId(), equalTo(artistWithId.getId()));
        assertThat(artist3.getWebsite(), equalTo(website));
    }

    @Test
    public void shouldDelete() throws Exception {
        shouldDelete(artists, artistWithId, artistWithId2, artistRepositoryMock, artistService);
    }

    @Test
    public void shouldFindById() throws Exception {
        shouldFindById(artistRepositoryMock, artistWithId, artistService);
    }

    @Test
    public void shouldFindAll() throws Exception {
        shouldFindAll(artistRepositoryMock, artists, artistService);
    }

    @Test
    public void shouldFindByMbid() throws Exception {
        when(artistRepositoryMock.findByMbid(MUM_MBID)).thenReturn(artistWithId);

        final Artist artistByMbid = artistService.findByMbid(MUM_MBID);

        assertThat(artistByMbid, equalTo(artistWithId));
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        shouldDeleteAll(artists, artistRepositoryMock, artistService);
    }

    @Test
    public void shouldFindByName() throws Exception {
        final List<Artist> artistList = Collections.singletonList(artistWithId);
        when(artistRepositoryMock.findByName(MUM_NAME)).thenReturn(artistList);

        final List<Artist> artistsByName = artistService.findByName(MUM_NAME);

        assertThat(artistsByName, hasSize(1));
        assertThat(artistsByName, hasItem(artistWithId));
    }

    @Test
    public void shouldCreateMany() throws Exception {
        final List<Artist> artistList = Arrays.asList(ARTIST_MUM, ARTIST_PETER_BRODERICK);

        when(artistRepositoryMock.create(PETER_BRODERICK_NAME, PETER_BRODERICK_MBID, null)).thenReturn(artistWithId2);

        final List<Artist> createdArtists = artistService.createMany(artistList);

        assertThat(createdArtists, hasSize(2));
        assertThat(createdArtists, hasItems(artistWithId, artistWithId2));
    }
}